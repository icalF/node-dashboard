var config = require('./config'),
    express = require('express'),
    passport = require('passport'),
    bodyParser = require('body-parser');

module.exports = function() {
  var app = express();

  app.use(passport.initialize());

  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());

  // require('../app/routes/index.server.routes.js')(app);
  require('../app/routes/contacts.server.routes.js')(app);
  require('../app/routes/users.server.routes.js')(app);

  app.use(express.static('./public'));

  return app;
};