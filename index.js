var config = require('./config/config'),
    mongoose = require('./config/mongoose'),
    express = require('./config/express');

var db = mongoose(),
    app = express();

app.listen(config.port);

module.exports = app;
console.log(config.node_env  + ' server running at http://localhost:' + config.port);  