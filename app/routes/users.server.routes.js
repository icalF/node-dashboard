var authController = require('../../app/controllers/auth.server.js'),
    users = require('../../app/controllers/users.server.controller');

module.exports = function(app) {
  app.route('/api/users')
     .post(authController.isAuthenticated, users.create)
     .get(authController.isAuthenticated, users.list);
  app.route('/api/users/:userId')
     .get(authController.isAuthenticated, users.read)
     .put(authController.isAuthenticated, users.update)
     .delete(authController.isAuthenticated, users.delete);
  app.param('userId', users.userByID);
};