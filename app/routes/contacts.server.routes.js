var authController = require('../../app/controllers/auth.server.js'),
    contacts = require('../../app/controllers/contacts.server.controller');

module.exports = function(app) {
  app.route('/api/contacts')
     .post(authController.isAuthenticated, contacts.create)
     .get(authController.isAuthenticated, contacts.list);
  app.route('/api/contacts/:userId')
     .get(authController.isAuthenticated, contacts.read)
     .put(authController.isAuthenticated, contacts.update)
     .delete(authController.isAuthenticated, contacts.delete);
  app.param('userId', contacts.contactByID);
};