var ContactModel = require('../../app/models/contact.server.model');

exports.create = function(req, res, next) {
  var contact = new ContactModel(req.body);
  contact.save(function(err, contact) {
    if (err) {
      return res.send(err);
    }
    else {
      res.json(contact);
    }
  });
};

exports.list = function(req, res, next) {
  ContactModel.find({}, function(err, contacts) {
    if (err) {
      return res.send(err);
    }
    else {
      res.json(contacts);
    }
  });
};

exports.read = function(req, res) {
  res.json(req.contact);
};

exports.contactByID = function(req, res, next, id) {
  ContactModel.findOne({
    _id: id
    },
    function(err, contact) {
      if (err) {
        return res.send(err);
      }
      else {
        req.contact = contact;
        next();
      }
    }
  );
};

exports.update = function(req, res, next) {
  ContactModel.findByIdAndUpdate(req.contact.id, req.body, 
    function(err, contact) {
      if (err) {
        return res.send(err);
      }
      else {
        res.json(contact);
      }
    }
  );
};

exports.delete = function(req, res, next) {
  ContactModel.remove(req.contact, function(err, result) {
    if (err) {
      return res.send(err);
    }
    else {
      res.json(result);
    }
  })
};