var UserModel = require('../../app/models/user.server.model');

exports.create = function(req, res, next) {
  var user = new UserModel(req.body);
  user.save(function(err, user) {
    if (err) {
      return res.send(err);
    }
    else {
      res.json(user);
    }
  });
};

exports.list = function(req, res, next) {
  UserModel.find({}, function(err, users) {
    if (err) {
      return res.send(err);
    }
    else {
      res.json(users);
    }
  });
};

exports.read = function(req, res) {
  res.json(req.user);
};

exports.userByID = function(req, res, next, id) {
  UserModel.findOne({
    _id: id
    },
    function(err, user) {
      if (err) {
        return res.send(err);
      }
      else {
        req.user = user;
        next();
      }
    }
  );
};

exports.update = function(req, res, next) {
  UserModel.findByIdAndUpdate(req.user.id, req.body, 
    function(err, user) {
      if (err) {
        return res.send(err);
      }
      else {
        res.json(user);
      }
    }
  );
};

exports.delete = function(req, res, next) {
  UserModel.remove(req.user, function(err, result) {
    if (err) {
      return res.send(err);
    }
    else {
      res.json(result);
    }
  })
};