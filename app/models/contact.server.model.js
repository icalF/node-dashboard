var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ContactSchema = new Schema({
    name: { type: String, required: true },
    title: { type: String, required: false },
    email: { type: String, required: false },
    phone: { type: Number, required: false },
    address: { type: String, required: false },
    company: { type: String, required: false },
    createdAt: { type: Date, default: Date.now },
});

var ContactModel = mongoose.model('Contact', ContactSchema);

module.exports = ContactModel;