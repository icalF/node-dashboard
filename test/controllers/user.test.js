var should = require('chai').should(),
    sinon = require('sinon'),
    mongoose = require('mongoose');

require('sinon-mongoose');

var UserModel = require('../../app/models/user.server.model');
var UserController = require('../../app/controllers/users.server.controller');

describe("Unit test for user API", function() {
  var UserMock;

  beforeEach(function() {
    UserMock = sinon.mock(UserController)
  });

  describe("Create a new user", function() {
    it("should create new user", function(done) {
      var UserMock = sinon.mock(UserController);
      var expectedResult = { status: true, id: 1234 };
      UserMock.expects('create').yields(null, expectedResult);
      UserController.create(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        result.status.should.to.be.true;
        result.id.should.to.be.equal(1234);
        done();
      });
    });

    it("should return error, if user not saved", function(done) {
      var UserMock = sinon.mock(UserController);
      var expectedResult = { status: false };
      UserMock.expects('create').yields(expectedResult, null);
      UserController.create(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        err.status.should.not.to.be.true;
        done();
      });
    });
  });

  describe("Read all users", function() {
    it("should return all users", function(done) {
      
      var expectedResult = {status: true, user: {
        Bambang: 'Gentolet'}
      };
      UserMock.expects('list').yields(null, expectedResult);
      UserController.list(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        result.status.should.to.be.true;
        result.user.should.deep.equal({Bambang: 'Gentolet'})
        done();
      });
    });

    it("should return error", function(done) {
      var expectedResult = {status: false};
      UserMock.expects('list').yields(expectedResult, null);
      UserController.list(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        err.status.should.not.to.be.true;
        done();
      });
    });
  });

  describe("Update a new user by id", function() {
    it("should updated a user by id", function(done) {
      var expectedResult = { status: true, user: [
        'Bambang', 'Gentolet'
      ]};
      UserMock.expects('update').yields(null, expectedResult);
      UserController.update(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        result.status.should.to.be.true;
        result.user.should.deep.equal([
          'Bambang', 'Gentolet'
        ]);
        done();
      });
    });

    it("should return error if update action is failed", function(done) {
      var expectedResult = { status: false };
      UserMock.expects('update').yields(expectedResult, null);
      UserController.update(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        err.status.should.not.to.be.true;
        done();
      });
    });
  });

  describe("Delete a user by id", function() {
    it("should delete a user by id", function(done) {
      var expectedResult = { status: true };
      UserMock.expects('delete').yields(null, expectedResult);
      UserController.delete(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        result.status.should.to.be.true;
        done();
      });
    });

    it("should return error if delete action is failed", function(done) {
      var expectedResult = { status: false };
      UserMock.expects('delete').yields(expectedResult, null);
      UserController.delete(function (err, result) {
        UserMock.verify();
        UserMock.restore();
        err.status.should.not.to.be.true;
        done();
      });
    });
  });

  describe("Authenticate user", function() {
    it("should verify if password matched", function() {
      var user = new UserModel({ username: "raja", password: "solo" })
      user.verifyPassword("solo", function(err, result) {
        status.should.to.be.true;
      });
    });

    it("should verify if password not matched", function() {
      var user = new UserModel({ username: "raja", password: "solo" })
      user.verifyPassword("gazandi", function(err, result) {
        status.should.not.to.be.true;
      });
    });
  });
})