var should = require('chai').should(),
    sinon = require('sinon'),
    mongoose = require('mongoose');

require('sinon-mongoose');

var ContactController = require('../../app/controllers/contacts.server.controller');

describe("Unit test for contact API", function() {
  var ContactMock;

  beforeEach(function() {
    ContactMock = sinon.mock(ContactController)
  });

  describe("Create a new contact", function() {
    // Test will pass if the contact is saved
    it("should create new contact", function() {
      var ContactMock = sinon.mock(ContactController);
      var expectedResult = { status: true, id: 1234 };
      ContactMock.expects('create').yields(null, expectedResult);
      ContactController.create(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        result.status.should.to.be.true;
        result.id.should.to.be.equal(1234);
      });
    });

    // Test will pass if the contact is not saved
    it("should return error, if contact not saved", function() {
      var ContactMock = sinon.mock(ContactController);
      var expectedResult = { status: false };
      ContactMock.expects('create').yields(expectedResult, null);
      ContactController.create(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        err.status.should.not.to.be.true;
      });
    });
  });

  describe("Read all contacts", function() {
    // Test will pass if we get all contacts
    it("should return all contacts", function() {
      
      var expectedResult = {status: true, contact: {
        Bambang: 'Gentolet'}
      };
      ContactMock.expects('list').yields(null, expectedResult);
      ContactController.list(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        result.status.should.to.be.true;
        result.contact.should.deep.equal({Bambang: 'Gentolet'})
      });
    });

    // Test will pass if we fail to get a contact
    it("should return error", function() {
      var expectedResult = {status: false};
      ContactMock.expects('list').yields(expectedResult, null);
      ContactController.list(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        err.status.should.not.to.be.true;
      });
    });
  });

  describe("Update a new contact by id", function() {
    // Test will pass if the contact is updated based on an ID
    it("should updated a contact by id", function() {
      var expectedResult = { status: true, contact: [
        'Bambang', 'Gentolet'
      ]};
      ContactMock.expects('update').yields(null, expectedResult);
      ContactController.update(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        result.status.should.to.be.true;
        result.contact.should.deep.equal([
          'Bambang', 'Gentolet'
        ]);
      });
    });

    // Test will pass if the contact is not updated based on an ID
    it("should return error if update action is failed", function() {
      var expectedResult = { status: false };
      ContactMock.expects('update').yields(expectedResult, null);
      ContactController.update(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        err.status.should.not.to.be.true;
      });
    });
  });

  describe("Delete a contact by id", function() {
    // Test will pass if the contact is deleted based on an ID
    it("should delete a contact by id", function() {
      var expectedResult = { status: true };
      ContactMock.expects('delete').yields(null, expectedResult);
      ContactController.delete(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        result.status.should.to.be.true;
      });
    });

    // Test will pass if the contact is not deleted based on an ID
    it("should return error if delete action is failed", function() {
      var expectedResult = { status: false };
      ContactMock.expects('delete').yields(expectedResult, null);
      ContactController.delete(function (err, result) {
        ContactMock.verify();
        ContactMock.restore();
        err.status.should.not.to.be.true;
      });
    });
  });
})