var chai = require('chai'),
    should = chai.should(),
    server = require('../index'),
    chaiHttp = require('chai-http');

chai.use(chaiHttp);

var ContactModel = require('../app/models/contact.server.model');
var UserModel = require('../app/models/user.server.model');

describe("Integration test for contact API", function() {
  var ContactMock, id,
      contact = { title: "Mr", phone: 1234 },
      user;

  // registering user
  before(function(done) {
    user = new UserModel({ username: 'user', password: 'secret'});
    user.save(function() { done(); });
  });

  // unregistering user
  after(function(done) {
    UserModel.remove({}, function() { done(); });
  })

  it("should return no contact", function(done) {
    chai.request(server)
        .get('/api/contacts')
        .auth(user.username, user.password)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(0);
          done();
        });
  });

  it("should not create new incomplete contact", function(done) {
    chai.request(server)
        .post('/api/contacts')
        .auth(user.username, user.password)
        .send(contact)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('errors').a('object');
          res.body.errors.name.should.have.property('path').eql('name');
          res.body.errors.name.should.have.property('kind').eql('required');
          done();
        });
  });

  it("should create new contact", function(done) {
    contact.name = "Naruto";
    chai.request(server)
        .post('/api/contacts')
        .auth(user.username, user.password)
        .send(contact)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').eql(contact.name);
          res.body.should.not.have.property('email');
          res.body.should.have.property('phone').eql(contact.phone);
          res.body.should.have.property('_id');
          id = res.body._id;
          done();
        });
  });

  it("should return contact by id", function(done) {
    chai.request(server)
        .get('/api/contacts/' + id)
        .auth(user.username, user.password)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').eql(contact.name);
          res.body.should.not.have.property('email');
          res.body.should.have.property('phone').eql(contact.phone);
          res.body.should.have.property('title').eql(contact.title);
          done();
        });
  });

  it("should success updating a contact by id", function(done) {
    var newContact = {
      name: "Naruto",
      phone: 2345,
      email: "naruto@konoha.or.jp",
      title: "Dainana"
    };
    chai.request(server)
        .put('/api/contacts/' + id)
        .auth(user.username, user.password)
        .send(newContact)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('title').eql(contact.title);
          contact = newContact;

          chai.request(server)
              .get('/api/contacts/' + id)
              .auth(user.username, user.password)
              .end(function(err, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name').eql(contact.name);
                res.body.should.have.property('email').eql(contact.email);
                res.body.should.have.property('phone').eql(contact.phone);
                res.body.should.have.property('title').eql(contact.title);
                done();
              });
        });
  });    

  it("should delete a contact by id", function(done) {
    chai.request(server)
        .delete('/api/contacts/' + id)
        .auth(user.username, user.password)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('ok').eql(1);
          res.body.should.have.property('n').eql(1);
          done();
        });
  });

  it("should return no contact", function(done) {
    chai.request(server)
        .get('/api/contacts')
        .auth(user.username, user.password)
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(0);
          done();
        });
  });
});